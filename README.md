# textlint-es

[![Docker Build Status](https://img.shields.io/docker/build/ksugihara/textlint-es.svg)](https://hub.docker.com/r/ksugihara/textlint-es/)
[![Docker Pulls](https://img.shields.io/docker/pulls/ksugihara/textlint-es.svg)](https://hub.docker.com/r/ksugihara/textlint-es/)


## 概要

textlintをGitLabCIで実行するためのDockerImageです。

## 構成

GitLab上の当プロジェクトをコミットすると、Bitbucket上で杉原個人ユーザが所有している同名プロジェクトにミラーリングされます。
そしてBitbucketのプロジェクトにコミットされるとDockerに連携され、イメージのビルドが行われます。

## 使い方
`.gitlab-ci.yml`にてtextlintを実行する際のimage名に`ksugihara/textlint-es`を指定します。

### 例
```
stages:
  - lint

textlint:
  image: ksugihara/textlint-es
  stage: lint
  script:
    - textlint Documents/
  only:
    - /^revision\/.*$/

```

## 含まれるtextlintルール
ルール | 概要
--- | ---
preset-ja-technical-writing | 技術文書向けのtextlintルールプリセット
preset-jtf-style | JTF日本語標準スタイルガイド
preset-ja-spacing | スペース周りのスタイル
spellcheck-tech-word | IT技術用語辞書
prh | 別途定義したルールファイルに基づくチェック

## 注意
BitbuckeetリポジトリもDockerリポジトリも公開状態にしてありますので、記述内容には留意してください。